Pod::Spec.new do |s|

  s.name         =  'MXCoreData'
  s.version      =  "2.3.7"
  s.summary      =  'Core Data framework.'
  s.homepage     =  'http://www.mobext.com'
  s.author       =  { 'Dino' => 'dino@mobext.com', 'Michael Loistl' => 'michael.loistl@mobext.com' }
  s.source       =  { :git => 'https://bitbucket.org/mobext/mxcoredata.git', :branch => 'master', :tag => '2.3.7' }
  s.license      =  'Intern'
  
  # Platform setup
  s.requires_arc = true
  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.7'
  
  s.source_files = 'MXCoreData/**/*.{h,m}'
  s.framework  = 'CoreData'
  
  s.dependency 'AFNetworking', '~> 2.3'

  s.prefix_header_contents = <<-EOS
#import <Availability.h>

#if __IPHONE_OS_VERSION_MIN_REQUIRED
  #import <SystemConfiguration/SystemConfiguration.h>
  #import <MobileCoreServices/MobileCoreServices.h>
  #import <Security/Security.h>
#else
  #import <SystemConfiguration/SystemConfiguration.h>
  #import <CoreServices/CoreServices.h>
  #import <Security/Security.h>
#endif
EOS

end