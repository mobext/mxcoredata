//
//  NSManagedObject+Mapping.h
//  Framework
//
//  Created by Michael Loistl on 17/03/2014.
//
//

#import <CoreData/CoreData.h>

typedef void (^MXMappingPreProcessingBlock)(id dataObject, NSManagedObject *managedObject, NSString *path, NSString *httpMethod, NSString *identifier);
typedef void (^MXMappingPostProcessingBlock)(id dataObject, NSManagedObject *managedObject, NSString *path, NSString *httpMethod, NSString *identifier, BOOL mapped);

@interface NSManagedObject (Mapping)

#pragma mark - Methods

+ (NSDictionary *)primaryKeysForPath:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

+ (NSString *)dataKeyPathForPath:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

+ (NSDictionary *)mappingForPath:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

+ (BOOL)shouldMapManagedObject:(NSManagedObject *)managedObject dataObject:(id)dataObject primaryKeys:(NSDictionary *)primaryKeys mapping:(NSDictionary *)mapping httpMethod:(NSString *)httpMethod path:(NSString *)path identifier:(NSString *)identifier inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

#pragma mark - Mapping

- (MXMappingPreProcessingBlock)preProcessingBlock;
- (MXMappingPostProcessingBlock)postProcessingBlock;

+ (NSArray *)mapManagedObjectID:(NSManagedObjectID *)managedObjectID
                       withJSON:(id)JSON
                     identifier:(NSString *)identifier
                     httpMethod:(NSString *)httpMethod
                           path:(NSString *)path
           managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSManagedObject *)mapManagedObjectID:(NSManagedObjectID *)managedObjectID
                         withDataObject:(id)dataObject
                            primaryKeys:(NSDictionary *)primaryKeys
                                mapping:(NSDictionary *)mapping
                             httpMethod:(NSString *)httpMethod
                                   path:(NSString *)path
                             identifier:(NSString *)identifier
                 inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

@end
