//
//  NSManagedObject+Networking.h
//  Framework
//
//  Created by Michael Loistl on 17/03/2014.
//
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Networking)

#pragma mark - Methods

+ (NSString *)pathForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSString *)pathExtensionForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier;
+ (NSString *)pathExtensionForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSDictionary *)parametersForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier;
+ (NSDictionary *)parametersForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

#pragma mark - URLSessionDataTasks

+ (void)GETWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

- (void)POSTWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

- (void)PUTWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

- (void)DELETEWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSManagedObjectContext *managedObjectContext))completion;

- (void)SYNCWithHTTPMethod:(NSString *)httpMethod
                completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

+ (void)GET:(NSString *)path
pathExtension:(NSString *)pathExtension
 parameters:(NSDictionary *)parameters
 completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

- (void)POST:(NSString *)path
pathExtension:(NSString *)pathExtension
  parameters:(NSDictionary *)parameters
  completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

- (void)PUT:(NSString *)path
pathExtension:(NSString *)pathExtension
 parameters:(NSDictionary *)parameters
 completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

- (void)DELETE:(NSString *)path
 pathExtension:(NSString *)pathExtension
    parameters:(NSDictionary *)parameters
    completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSManagedObjectContext *managedObjectContext))completion;

- (void)sessionDataTaskWithPath:(NSString *)path
                  pathExtension:(NSString *)pathExtension
                     parameters:(NSDictionary *)parameters
                     httpMethod:(NSString *)httpMethod
                     completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

+ (void)sessionDataTaskWithPath:(NSString *)path
                  pathExtension:(NSString *)pathExtension
                     parameters:(NSDictionary *)parameters
                     httpMethod:(NSString *)httpMethod
                managedObjectID:(NSManagedObjectID *)managedObjectID
                            map:(BOOL)map
      mapInManagedObjectContext:(NSManagedObjectContext *)mapInManagedObjectContext
              mappingIdentifier:(NSString *)mappingIdentifier
                     completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion;

@end
