//
//  NSManagedObject+Networking.m
//  Framework
//
//  Created by Michael Loistl on 17/03/2014.
//
//

#import "NSManagedObject+Networking.h"
#import "NSManagedObject+Mapping.h"
#import "NSManagedObject+CoreDataManager.h"
#import "NSManagedObjectContext+CoreDataManager.h"
#import "MXCoreDataManager.h"
#import "MXHTTPSessionManager.h"

@implementation NSManagedObject (Networking)

#pragma mark - Methods

+ (NSString *)pathForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    // Path
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(pathForManagedObjectClass:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate pathForManagedObjectClass:self httpMethod:httpMethod identifier:identifier];
    }
    
    return nil;
}

- (NSString *)pathExtensionForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    
    // PathExtension
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(pathExtensionForManagedObject:withManagedObjectClass:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate pathExtensionForManagedObject:self withManagedObjectClass:[self class] httpMethod:httpMethod identifier:identifier];
    }
    
    return nil;
}

+ (NSString *)pathExtensionForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    
    // PathExtension
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(pathExtensionForManagedObject:withManagedObjectClass:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate pathExtensionForManagedObject:nil withManagedObjectClass:[self class] httpMethod:httpMethod identifier:identifier];
    }
    
    return nil;
}

- (NSDictionary *)parametersForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(parametersForManagedObject:withManagedObjectClass:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate parametersForManagedObject:self withManagedObjectClass:[self class] httpMethod:httpMethod identifier:identifier];
    }
    return nil;
}

+ (NSDictionary *)parametersForHTTPMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(parametersForManagedObject:withManagedObjectClass:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate parametersForManagedObject:nil withManagedObjectClass:self httpMethod:httpMethod identifier:identifier];
    }
    return nil;
}

#pragma mark - URLSessionDataTasks

+ (void)GETWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    NSString *path = [self pathForHTTPMethod:kHTTPMethodGET identifier:nil];
    NSString *pathExtension = [self pathExtensionForHTTPMethod:kHTTPMethodGET identifier:nil];
    NSDictionary *parameters = [self parametersForHTTPMethod:kHTTPMethodGET identifier:nil];
    
    if (path) {
        [self GET:path pathExtension:pathExtension parameters:parameters completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
            completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
        }];
    } else {
        completion(nil, nil, nil, NO, nil, nil, nil, nil);
    }
}

- (void)POSTWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    NSString *path = [[self class] pathForHTTPMethod:kHTTPMethodGET identifier:nil];
    NSString *pathExtension = [self pathExtensionForHTTPMethod:kHTTPMethodGET identifier:nil];
    NSDictionary *parameters = [self parametersForHTTPMethod:kHTTPMethodPOST identifier:nil];
    
    if (path) {
        [self POST:path pathExtension:pathExtension parameters:parameters completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
            completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
        }];
    } else {
        completion(nil, nil, nil, NO, nil, nil, nil, nil);
    }
}

- (void)PUTWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    NSString *path = [[self class] pathForHTTPMethod:kHTTPMethodPUT identifier:nil];
    NSString *pathExtension = [self pathExtensionForHTTPMethod:kHTTPMethodPUT identifier:nil];
    NSDictionary *parameters = [self parametersForHTTPMethod:kHTTPMethodPUT identifier:nil];
    
    if (path) {
        [self PUT:path pathExtension:pathExtension parameters:parameters completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
            completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
        }];
    } else {
        completion(nil, nil, nil, NO, nil, nil, nil, nil);
    }
}

- (void)DELETEWithCompletion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSManagedObjectContext *managedObjectContext))completion {
    
    NSString *path = [[self class] pathForHTTPMethod:kHTTPMethodDELETE identifier:nil];
    NSString *pathExtension = [self pathExtensionForHTTPMethod:kHTTPMethodDELETE identifier:nil];
    NSDictionary *parameters = [self parametersForHTTPMethod:kHTTPMethodDELETE identifier:nil];
    
    if (path) {
        [self DELETE:path pathExtension:pathExtension parameters:parameters completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSManagedObjectContext *managedObjectContext) {
            completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, managedObjectContext);
        }];
    } else {
        completion(nil, nil, nil, NO, nil, nil, nil);
    }
}

- (void)SYNCWithHTTPMethod:(NSString *)httpMethod completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [self.managedObjectContext performBlock:^{
            NSString *path = [[self class] pathForHTTPMethod:httpMethod identifier:nil];
            NSString *pathExtension = [self pathExtensionForHTTPMethod:httpMethod identifier:nil];
            NSDictionary *parameters = [self parametersForHTTPMethod:httpMethod identifier:nil];
            
            if (path) {
                [self sessionDataTaskWithPath:path pathExtension:pathExtension parameters:parameters httpMethod:httpMethod completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
                    
                    completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
                }];
            } else {
                completion(nil, nil, nil, NO, nil, nil, nil, nil);
            }
        }];
        
    });
}

+ (void)GET:(NSString *)path pathExtension:(NSString *)pathExtension parameters:(NSDictionary *)parameters completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    NSManagedObjectContext *managedObjectContext = [MXCoreDataManager managedObjectContextBackground];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [managedObjectContext performBlock:^{
            [self sessionDataTaskWithPath:path pathExtension:pathExtension parameters:parameters httpMethod:kHTTPMethodGET managedObjectID:nil map:YES mapInManagedObjectContext:managedObjectContext mappingIdentifier:nil completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
                completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
            }];
        }];
    });
}

- (void)POST:(NSString *)path pathExtension:(NSString *)pathExtension parameters:(NSDictionary *)parameters completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.managedObjectContext performBlock:^{
            [self sessionDataTaskWithPath:path pathExtension:pathExtension parameters:parameters httpMethod:kHTTPMethodPOST completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
                completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
            }];
        }];
    });
}

- (void)PUT:(NSString *)path pathExtension:(NSString *)pathExtension parameters:(NSDictionary *)parameters completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.managedObjectContext performBlock:^{
            [self sessionDataTaskWithPath:path pathExtension:pathExtension parameters:parameters httpMethod:kHTTPMethodPUT completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
                completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
            }];
        }];
    });
}

- (void)DELETE:(NSString *)path pathExtension:(NSString *)pathExtension parameters:(NSDictionary *)parameters completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSManagedObjectContext *managedObjectContext))completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.managedObjectContext performBlock:^{
            [self sessionDataTaskWithPath:path pathExtension:pathExtension parameters:parameters httpMethod:kHTTPMethodDELETE completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
                completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, managedObjectContext);
            }];
        }];
    });
}

- (void)sessionDataTaskWithPath:(NSString *)path pathExtension:(NSString *)pathExtension parameters:(NSDictionary *)parameters httpMethod:(NSString *)httpMethod completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    [self.managedObjectContext performBlock:^{
        [[self class] sessionDataTaskWithPath:path pathExtension:pathExtension parameters:parameters httpMethod:httpMethod managedObjectID:self.objectID map:YES mapInManagedObjectContext:[MXCoreDataManager managedObjectContextBackground] mappingIdentifier:nil completion:^(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext) {
            completion(sessionDataTask, requestDate, respondDate, success, error, responseObject, mappedManagedObjects, managedObjectContext);
        }];
    }];
}

+ (void)sessionDataTaskWithPath:(NSString *)path pathExtension:(NSString *)pathExtension parameters:(NSDictionary *)parameters httpMethod:(NSString *)httpMethod managedObjectID:(NSManagedObjectID *)managedObjectID map:(BOOL)map mapInManagedObjectContext:(NSManagedObjectContext *)mapInManagedObjectContext mappingIdentifier:(NSString *)mappingIdentifier completion:(void (^)(NSURLSessionDataTask *sessionDataTask, NSDate *requestDate, NSDate *respondDate, BOOL success, NSError *error, id responseObject, NSArray *mappedManagedObjects, NSManagedObjectContext *managedObjectContext))completion {
    
    MXCoreDataManager *coreDataManager = [MXCoreDataManager sharedManager];
    MXHTTPSessionManager *sessionManager = [MXHTTPSessionManager sharedManager];
    
    // If network is currently reachable
//    NSLog(@"STATUS: %ld", sessionManager.reachabilityManager.networkReachabilityStatus);
    
    if (sessionManager.reachabilityManager.networkReachabilityStatus > 0) {
        NSError *error = nil;
        NSString *requestPath = (pathExtension.length > 0) ? [path stringByAppendingString:pathExtension] : path;
        
        NSMutableURLRequest *request = [sessionManager.requestSerializer requestWithMethod:httpMethod URLString:[[NSURL URLWithString:requestPath relativeToURL:sessionManager.baseURL] absoluteString] parameters:parameters error:&error];
        
        NSDate *requestDate = [NSDate date];
        
        __block NSURLSessionDataTask *sessionDataTask = [sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
            
            NSDate *responseDate = [NSDate date];
            
            NSHTTPURLResponse *httpURLResponse = (NSHTTPURLResponse *)response;
            BOOL success = ([httpURLResponse statusCode] >= 200 &&
                            [httpURLResponse statusCode] < 300) ? YES : NO;
            
            //                    NSLog(@"PATH: %@ - %@", httpMethod, request.URL.absoluteString);
            //                    NSLog(@"PARAMETERS: %@ - %@", httpMethod, parameters);
            //                    NSLog(@"RESPONSE: (%ld) %@", (long)httpURLResponse.statusCode, responseObject);
            
            if (success) {
                
                // Delegate
                if ([(NSObject *)coreDataManager.delegate respondsToSelector:@selector(coreDataManager:didRespondToRequestForManagedObjectClass:withSessionDataTask:path:parameters:responseObject:)]) {
                    [coreDataManager.delegate coreDataManager:coreDataManager didRespondToRequestForManagedObjectClass:self withSessionDataTask:sessionDataTask path:path parameters:parameters responseObject:responseObject];
                }
                NSArray *mappedManagedObjects;
                if (map) {
                    mappedManagedObjects = [self mapManagedObjectID:managedObjectID withJSON:responseObject identifier:mappingIdentifier httpMethod:httpMethod path:path managedObjectContext:mapInManagedObjectContext];
                }
                
                completion(sessionDataTask, requestDate, responseDate, success, error, responseObject, mappedManagedObjects, mapInManagedObjectContext);
            } else {
                [self handleErrorForResponse:httpURLResponse
                              responseObject:responseObject
                             managedObjectID:managedObjectID
                                        path:path
                                  parameters:parameters
                                  httpMethod:httpMethod];
                completion(sessionDataTask, requestDate, responseDate, success, error, responseObject, nil, mapInManagedObjectContext);
            }
            
        }];
        
        [sessionDataTask resume];
    } else {
        completion(nil, nil, nil, NO, nil, nil, nil, nil);
    }
}

+ (void)handleErrorForResponse:(NSHTTPURLResponse *)httpURLResponse responseObject:(id)responseObject managedObjectID:(NSManagedObjectID *)managedObjectID path:(NSString *)path parameters:(NSDictionary *)parameters httpMethod:(NSString *)httpMethod {
    
//    MXHTTPSessionManager *sessionManager = [MXHTTPSessionManager sharedManager];
////    
//    // Cancel all queued Operations
//    [sessionManager cancelAllDataTasks];
}

@end
