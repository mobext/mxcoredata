//
//  MXHTTPSessionManager.m
//  Framework
//
//  Created by Michael Loistl on 17/03/2014.
//  
//

#import "MXHTTPSessionManager.h"
#import "MXCoreDataManager.h"

@implementation MXHTTPSessionManager

#pragma mark - MLHTTPSessionManager

+ (MXHTTPSessionManager *)sharedManager {
    @synchronized([MXHTTPSessionManager class]){
        static MXHTTPSessionManager *_sharedManager = nil;
        static dispatch_once_t oncePredicate;
        
        dispatch_once(&oncePredicate, ^{
            
            NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
            sessionConfiguration.allowsCellularAccess = YES;
            
            _sharedManager = [[MXHTTPSessionManager alloc] initWithBaseURL:[MXCoreDataManager sharedManager].baseURL sessionConfiguration:sessionConfiguration];
            _sharedManager.completionQueue = [MXCoreDataManager sharedManager].backgroundQueue;
            _sharedManager.operationQueue.maxConcurrentOperationCount = 10;
            
            [_sharedManager.reachabilityManager startMonitoring];
            [_sharedManager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
                
                MXCoreDataManager *coreDataManager = [MXCoreDataManager sharedManager];
                
                // Delegate
                if ([(NSObject *)coreDataManager.delegate respondsToSelector:@selector(coreDataManager:didChangeNetworkReachabilityStatus:)]) {
                    [coreDataManager.delegate coreDataManager:coreDataManager didChangeNetworkReachabilityStatus:status];
                }
                
            }];
            
        });
        
        return _sharedManager;
    }
}

- (void)cancelAllDataTasks {
    NSArray *sessionDataTasks = [self.tasks copy];
    
    for (NSURLSessionDataTask *sessionDataTask in sessionDataTasks) {
        if (sessionDataTask.state != NSURLSessionTaskStateCanceling) {
            [sessionDataTask cancel];
        }
    }
}

- (void)suspendAllDataTasks {
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        self.suspended = YES;
        
        for (NSURLSessionDataTask *sessionDataTask in dataTasks) {
            if (sessionDataTask.state == NSURLSessionTaskStateRunning) {
                [sessionDataTask suspend];
            }
        }
    }];
}

- (void)resumeAllDataTasks {
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        for (NSURLSessionDataTask *sessionDataTask in dataTasks) {
            if (sessionDataTask.state == NSURLSessionTaskStateSuspended) {
                [sessionDataTask resume];
            }
        }
        self.suspended = NO;
    }];
}

- (void)sessionDataTaskIsQueuedForRequest:(NSURLRequest *)request httpMethod:(NSString *)httpMethod completion:(void (^)(BOOL requestIsQueued))completion {
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        BOOL requestIsQueued = NO;
        
        if ([httpMethod isEqualToString:@"GET"]) {
            for (NSURLSessionDataTask *sessionDataTask in dataTasks) {
                if ([sessionDataTask.originalRequest.URL.absoluteString isEqualToString:request.URL.absoluteString]) {
                    requestIsQueued = YES;
                    break;
                }
            }
        }
        
        completion(requestIsQueued);
    }];
}

@end
