//
//  MXCoreDataManager.h
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "NSManagedObject+CoreDataManager.h"
#import "NSManagedObjectContext+CoreDataManager.h"
#import "MXCoreDataManager.h"

@implementation NSManagedObject (CoreDataManager)

#pragma mark - Methods

- (NSDateFormatter *)dateFormatterForAttribute:(NSString *)attribute identifier:(NSString *)identifier  {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(dateFormatterForManagedObject:attribute:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate dateFormatterForManagedObject:self attribute:attribute identifier:identifier];
    }
    return nil;
}

#pragma mark - Default Context

- (void)delete {
    [self.managedObjectContext deleteObject:self];
}

+ (void)deleteAll {
    [self deleteAllInContext:[NSManagedObjectContext defaultContext]];
}

+ (void)deleteAllInEntity:(NSString *)entity {
    [self deleteAllInEntity:entity inContext:[NSManagedObjectContext defaultContext]];
}

+ (id)create {
    return [self createInContext:[NSManagedObjectContext defaultContext]];
}

+ (id)create:(NSDictionary *)attributes {
    return [self create:attributes inContext:[NSManagedObjectContext defaultContext]];
}

- (void)update:(NSDictionary *)attributes {
    [attributes enumerateKeysAndObjectsUsingBlock:^(NSString *attribute, id value, BOOL *stop) {
        [self setValue:value forAttribute:attribute withIdentifier:nil];
    }];
}

- (BOOL)setValue:(id)value forAttribute:(NSString *)attribute withIdentifier:(NSString *)identifier
{
    NSDictionary *entityAttributes = [self.entity attributesByName];
    NSDictionary *entityRelationships = [self.entity relationshipsByName];
    
    BOOL valueSet = NO;
    
    if ([entityAttributes objectForKey:attribute])
    {
        if (value != [NSNull null])
        {
            NSAttributeDescription *attributeDescription = [entityAttributes objectForKey:attribute];
            
            // Integer
            if (attributeDescription.attributeType == NSInteger16AttributeType ||
                attributeDescription.attributeType == NSInteger32AttributeType ||
                attributeDescription.attributeType == NSInteger64AttributeType)
            {
                if (! [value isKindOfClass:[NSNumber class]])
                {
                    value = @([value integerValue]);
                }
            }
            // Double
            else if (attributeDescription.attributeType == NSDecimalAttributeType ||
                     attributeDescription.attributeType == NSDoubleAttributeType ||
                     attributeDescription.attributeType == NSFloatAttributeType)
            {
                if ([value isKindOfClass:[NSDate class]]) {
                    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:value];
                    value = @(dateComponents.hour * 3600.0 + dateComponents.minute * 60.0 + dateComponents.second);
                } else if (! [value isKindOfClass:[NSNumber class]]) {
                    value = @([value doubleValue]);
                }
            }
            // String
            else if (attributeDescription.attributeType == NSStringAttributeType)
            {
                if ([value isKindOfClass:[NSSet class]]) {
                    NSString *stringValue = @"";
                    for (NSString *string in value) {
                        if (stringValue.length > 0) stringValue = [stringValue stringByAppendingString:@","];
                        stringValue = [stringValue stringByAppendingString:string];
                    }
                    value = stringValue;
                } else if (! [value isKindOfClass:[NSString class]]) {
                    value = [value stringValue];
                }
            }
            // Boolean
            else if (attributeDescription.attributeType == NSBooleanAttributeType)
            {
                if ([value isKindOfClass:[NSString class]])
                {
                    __block id boolValue = value;
                    
                    [@[@"1", @"True", @"true"] enumerateObjectsUsingBlock:^(NSString *yesValue, NSUInteger idx, BOOL *stop) {
                        
                        if ([value isEqualToString:yesValue])
                        {
                            boolValue = @YES;
                            *stop = YES;
                        }
                        
                    }];
                    
                    [@[@"0", @"False", @"false"] enumerateObjectsUsingBlock:^(NSString *noValue, NSUInteger idx, BOOL *stop) {
                        if ([value isEqualToString:noValue])
                        {
                            boolValue = @NO;
                            *stop = YES;
                        }
                    }];
                    
                    value = boolValue;
                }
                else if (! [value isKindOfClass:[NSNumber class]])
                {
                    NSInteger integerValue = [value integerValue];
                    
                    if (integerValue <= 0)
                    {
                        value = @NO;
                    }
                    else if (integerValue >= 1)
                    {
                        value = @YES;
                    }
                }
            }
            // Date
            else if (attributeDescription.attributeType == NSDateAttributeType)
            {
                if ([value isKindOfClass:[NSString class]])
                {
                    NSDateFormatter *dateFormatter = [self dateFormatterForAttribute:attribute identifier:identifier];
                    if (dateFormatter) {
                        value = [dateFormatter dateFromString:value];
                    } else if ([value integerValue] > 0) {
                        value = [NSDate dateWithTimeIntervalSince1970:[value integerValue]];
                    }
                }
                
                if ([value isKindOfClass:[NSNumber class]])
                {
                    value = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
                }
                
                if (! [value isKindOfClass:[NSDate class]])
                {
                    value = nil;
                }
            }
        }
        else
        {
            // NSLog(@"### Value for attribute %@ is <NULL> ###", attribute);
            value = nil;
        }
        
        [self setValue:value forKey:attribute];
        valueSet = YES;
    }
    else if ([entityRelationships objectForKey:attribute])
    {
        if (value != [NSNull null])
        {
            NSRelationshipDescription *relationshipDescription = [entityRelationships objectForKey:attribute];
            
            if ([relationshipDescription isToMany])
            {
                if ([value isKindOfClass:[NSArray class]])
                {
                    value = [NSSet setWithArray:value];
                }
                
                if ([value isKindOfClass:[NSSet class]])
                {
                    [self setValue:value forKey:attribute];
                    valueSet = YES;
                }
            }
            else
            {
                if ([value isKindOfClass:[NSManagedObject class]])
                {
                    [self setValue:value forKey:attribute];
                    valueSet = YES;
                } else if ([value isKindOfClass:[NSSet class]]) {
                    [self setValue:[[value allObjects] lastObject] forKey:attribute];
                    valueSet = YES;
                }
            }
        }
        else
        {
            // NSLog(@"### Value for attribute %@ is <NULL> ###", attribute);
            value = nil;
            
            [self setValue:value forKey:attribute];
            valueSet = YES;
        }
    }
    
    return valueSet;
}

+ (NSArray *)all {
    return [self allInContext:[NSManagedObjectContext defaultContext]];
}

+ (NSArray *)allInEntity:(NSString *)entity {
    return [self allInEntity:(NSString *)entity inContext:[NSManagedObjectContext defaultContext]];
}

+ (NSArray *)where:(id)condition {
    return [self where:condition inContext:[NSManagedObjectContext defaultContext]];
}

+ (NSArray *)where:(id)condition inEntity:(NSString *)entity {
    return [self where:condition inEntity:entity inContext:[NSManagedObjectContext defaultContext]];
}

+ (BOOL)key:(NSString *)key inEntity:(NSString *)entity {
    return [self key:key inEntity:entity inManagedObjectContext:[NSManagedObjectContext defaultContext]];
}

+ (BOOL)attribute:(NSString *)attribute inEntity:(NSString *)entity {
    return [self attribute:attribute inEntity:entity inManagedObjectContext:[NSManagedObjectContext defaultContext]];
}

+ (BOOL)relationship:(NSString *)relationship inEntity:(NSString *)entity {
    return [self relationship:relationship inEntity:entity inManagedObjectContext:[NSManagedObjectContext defaultContext]];
}

#pragma mark - Custom Context

+ (Class)classForEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    return NSClassFromString(entityDescription.managedObjectClassName);
}

+ (id)createInContext:(NSManagedObjectContext *)context {
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                         inManagedObjectContext:context];
}

+ (id)create:(NSDictionary *)attributes inContext:(NSManagedObjectContext *)context {
    NSManagedObject *newEntity = [self createInContext:context];
    [newEntity update:attributes];
    return newEntity;
}

+ (void)deleteAllInContext:(NSManagedObjectContext *)context {
    [[self allInContext:context] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj delete];
    }];
}

+ (void)deleteAllInEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context {
    [[NSManagedObject classForEntity:entity inContext:context] deleteAllInContext:context];
}

+ (NSArray *)allInContext:(NSManagedObjectContext *)context {
    return [self fetchWithPredicate:nil inContext:context];
}

+ (NSArray *)allInEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context {
    return [[NSManagedObject classForEntity:entity inContext:context] allInContext:context];
}

+ (NSArray *)where:(id)condition inContext:(NSManagedObjectContext *)context {
    NSPredicate *predicate = ([condition isKindOfClass:[NSPredicate class]]) ? condition : [self predicateFromStringOrDict:condition];
    return [self fetchWithPredicate:predicate inContext:context];
}

+ (NSArray *)where:(id)condition inEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context {
    return [[NSManagedObject classForEntity:entity inContext:context] where:condition inContext:context];
}

+ (BOOL)key:(NSString *)key inEntity:(NSString *)entity inManagedObjectContext:(NSManagedObjectContext *)context {
    BOOL exists = NO;
    
    exists = [self attribute:key inEntity:entity inManagedObjectContext:context];
    
    if (! exists) {
        exists = [self relationship:key inEntity:entity inManagedObjectContext:context];
    }
    
    return exists;
}

+ (BOOL)attribute:(NSString *)attribute inEntity:(NSString *)entity inManagedObjectContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    NSDictionary *attributes = [entityDescription attributesByName];
    
    if ([attributes objectForKey:attribute]) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)relationship:(NSString *)relationship inEntity:(NSString *)entity inManagedObjectContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    NSDictionary *relationships = [entityDescription relationshipsByName];
    
    if ([relationships objectForKey:relationship]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Naming

+ (NSString *)entityName {
    NSString *classPrefix = [MXCoreDataManager sharedManager].classPrefix;
    NSString *classString = NSStringFromClass(self);
    
    NSString *entityName = [classString substringFromIndex:classPrefix.length];
    
    return entityName;
}

#pragma mark - Private

+ (NSPredicate *)predicateFromDictionary:(NSDictionary *)conditions options:(NSComparisonPredicateOptions)options {
    NSMutableArray *predicates = [NSMutableArray array];
    
    [conditions enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        NSExpression *expressionKey = [NSExpression expressionForKeyPath:key];
        NSExpression *expressionValue = [NSExpression expressionForConstantValue:obj];
        
        NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression: expressionKey
                                                                    rightExpression: expressionValue
                                                                           modifier: NSDirectPredicateModifier
                                                                               type: NSEqualToPredicateOperatorType
                                                                            options: options];
        [predicates addObject:predicate];
        
    }];
    
    return [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
}

+ (NSPredicate *)predicateFromStringOrDict:(id)condition {
    if ([condition isKindOfClass:[NSString class]]) {
        return [NSPredicate predicateWithFormat:condition];
    } else if ([condition isKindOfClass:[NSDictionary class]]) {
        return [self predicateFromDictionary:condition options:0];
    }
    
    return nil;
}

+ (NSArray *)fetchWithPredicate:(NSPredicate *)predicate
                      inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [self createFetchRequestInContext:context];

    [request setPredicate:predicate];
    
    __block NSArray *fetchedObjects;

    NSError *error = nil;
    fetchedObjects = [context executeFetchRequest:request error:&error];

    return fetchedObjects.count > 0 ? fetchedObjects : nil;
}

+ (NSFetchRequest *)createFetchRequestInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request;
    
    if ([self entityName] && context) {
        request = [NSFetchRequest new];
        NSEntityDescription *entity = [NSEntityDescription entityForName:[self entityName]
                                                  inManagedObjectContext:context];
        [request setEntity:entity];
    }

    return request;
}

@end