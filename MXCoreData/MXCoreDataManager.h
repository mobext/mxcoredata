//
//  MXCoreDataManager.h
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+CoreDataManager.h"
#import "AFNetworkReachabilityManager.h"

// Networking HTTPMethods
#define kHTTPMethodGET @"GET"
#define kHTTPMethodPOST @"POST"
#define kHTTPMethodPUT @"PUT"
#define kHTTPMethodDELETE @"DELETE"

@class MXCoreDataManager;

@protocol MXCoreDataManagerDelegate <NSObject>

@optional
- (NSString *)pathForManagedObjectClass:(id)managedObjectClass httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSString *)pathExtensionForManagedObject:(NSManagedObject *)managedObject withManagedObjectClass:(id)managedObjectClass httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSDictionary *)parametersForManagedObject:(NSManagedObject *)managedObject withManagedObjectClass:(id)managedObjectClass httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSDictionary *)primaryKeysForManagedObjectClass:(id)managedObjectClass path:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSDictionary *)mappingForManagedObjectClass:(id)managedObjectClass path:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSString *)dataKeyPathForManagedObjectClass:(id)managedObjectClass path:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier;

- (NSDateFormatter *)dateFormatterForManagedObject:(NSManagedObject *)managedObject attribute:(NSString *)attribute identifier:(NSString *)identifier;

- (BOOL)shouldMapManagedObject:(NSManagedObject *)managedObject dataObject:(id)dataObject primaryKeys:(NSDictionary *)primaryKeys mapping:(NSDictionary *)mapping httpMethod:(NSString *)httpMethod path:(NSString *)path identifier:(NSString *)identifier inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

- (void)coreDataManager:(MXCoreDataManager *)sender didRespondToRequestForManagedObjectClass:(id)managedObjectClass withSessionDataTask:(NSURLSessionDataTask *)sessionDataTask path:(NSString *)path parameters:(NSDictionary *)parameters responseObject:(id)responseObject;

- (void)coreDataManager:(MXCoreDataManager *)sender didChangeNetworkReachabilityStatus:(AFNetworkReachabilityStatus)status;

@end

@interface MXCoreDataManager : NSObject

@property (nonatomic) dispatch_queue_t backgroundQueue;

@property (strong, nonatomic) NSString *storeName;
@property (strong, nonatomic) NSString *dataModelName;
@property (strong, nonatomic) NSString *classPrefix;
@property (strong, nonatomic) NSURL *baseURL;

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *memoryStoreCoordinator;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContextMain;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContextBackground;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContextMemory;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContextStore;

@property (nonatomic, strong) NSMutableSet *syncingManagedObjects;

@property (nonatomic, weak) id<MXCoreDataManagerDelegate> delegate;

+ (MXCoreDataManager *)sharedManager;
+ (NSManagedObjectContext *)managedObjectContextMain;
+ (NSManagedObjectContext *)managedObjectContextBackground;
+ (NSManagedObjectContext *)managedObjectContextMemory;

- (void)performFetchWithController:(NSFetchedResultsController *)fetchedResultsController;

@end