//
//  MXCoreData.h
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "MXCoreDataManager.h"
#import "MXHTTPSessionManager.h"

#import "NSManagedObject+CoreDataManager.h"
#import "NSManagedObject+Mapping.h"
#import "NSManagedObject+Networking.h"
#import "NSManagedObjectContext+CoreDataManager.h"