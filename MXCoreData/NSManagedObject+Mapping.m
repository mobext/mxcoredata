//
//  NSManagedObject+Mapping.m
//  Framework
//
//  Created by Michael Loistl on 17/03/2014.
//
//

#import "NSManagedObject+Mapping.h"
#import "NSManagedObject+CoreDataManager.h"
#import "NSManagedObjectContext+CoreDataManager.h"
#import "MXCoreDataManager.h"

@implementation NSManagedObject (Mapping)

#pragma mark - Methods

+ (NSDictionary *)primaryKeysForPath:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(primaryKeysForManagedObjectClass:path:pathExtension:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate primaryKeysForManagedObjectClass:self path:path pathExtension:pathExtension httpMethod:httpMethod identifier:identifier];
    }
    return nil;
}

+ (NSString *)dataKeyPathForPath:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(dataKeyPathForManagedObjectClass:path:pathExtension:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate dataKeyPathForManagedObjectClass:self path:path pathExtension:pathExtension httpMethod:httpMethod identifier:identifier];
    }
    return nil;
}

+ (NSDictionary *)mappingForPath:(NSString *)path pathExtension:(NSString *)pathExtension httpMethod:(NSString *)httpMethod identifier:(NSString *)identifier {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(mappingForManagedObjectClass:path:pathExtension:httpMethod:identifier:)]) {
        return [[MXCoreDataManager sharedManager].delegate mappingForManagedObjectClass:self path:path pathExtension:pathExtension httpMethod:httpMethod identifier:identifier];
    }
    return nil;
}

+ (BOOL)shouldMapManagedObject:(NSManagedObject *)managedObject dataObject:(id)dataObject primaryKeys:(NSDictionary *)primaryKeys mapping:(NSDictionary *)mapping httpMethod:(NSString *)httpMethod path:(NSString *)path identifier:(NSString *)identifier inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    if ([(NSObject *)[MXCoreDataManager sharedManager].delegate respondsToSelector:@selector(shouldMapManagedObject:dataObject:primaryKeys:mapping:httpMethod:path:identifier:inManagedObjectContext:)]) {
        return [[MXCoreDataManager sharedManager].delegate shouldMapManagedObject:managedObject dataObject:dataObject primaryKeys:primaryKeys mapping:mapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
    }
    
    return YES;
}


- (MXMappingPreProcessingBlock)preProcessingBlock {
    return nil;
}

- (MXMappingPostProcessingBlock)postProcessingBlock {
    return nil;
}

#pragma mark - Mapping

+ (NSArray *)mapManagedObjectID:(NSManagedObjectID *)managedObjectID
                       withJSON:(id)JSON
                     identifier:(NSString *)identifier
                     httpMethod:(NSString *)httpMethod
                           path:(NSString *)path
           managedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    
    __block NSMutableArray *mappedManagedObjects = [[NSMutableArray alloc] init];
    
    __block NSDictionary *primaryKeys = [self primaryKeysForPath:path pathExtension:nil httpMethod:httpMethod identifier:identifier];
    __block NSDictionary *mapping = [self mappingForPath:path pathExtension:nil httpMethod:httpMethod identifier:identifier];
    __block NSString *dataKeyPath = [self dataKeyPathForPath:path pathExtension:nil httpMethod:httpMethod identifier:identifier];
    
    //    NSLog(@"PRIMARY KEYS (%@): %@", path, primaryKeys);
    //    NSLog(@"MAPPING (%@): %@", path, mapping);
    //    NSLog(@"DATAKEYPATH %@ : %@", path, dataKeyPath);
    
    if (primaryKeys && mapping) {
        id jsonDataObject = JSON;
        
        if ([JSON isKindOfClass:[NSDictionary class]]) {
            jsonDataObject = (dataKeyPath) ? [JSON objectForKey:dataKeyPath] : JSON;
        }
        
        
        if ([jsonDataObject isKindOfClass:[NSArray class]]) {
            [jsonDataObject enumerateObjectsUsingBlock:^(id dataObject, NSUInteger idx, BOOL *stop) {
                
                NSManagedObject *mappedManagedObject = [self mapManagedObjectID:managedObjectID withDataObject:dataObject primaryKeys:primaryKeys mapping:mapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
                
                if (mappedManagedObject) {
                    [mappedManagedObjects addObject:mappedManagedObject];
                }
            }];
        } else if ([jsonDataObject isKindOfClass:[NSDictionary class]]) {
            
            NSManagedObject *mappedManagedObject = [self mapManagedObjectID:managedObjectID withDataObject:jsonDataObject primaryKeys:primaryKeys mapping:mapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
            
            if (mappedManagedObject) {
                [mappedManagedObjects addObject:mappedManagedObject];
            }
        }
    }
    
    return mappedManagedObjects;
}

+ (NSManagedObject *)mapManagedObjectID:(NSManagedObjectID *)managedObjectID withDataObject:(id)dataObject primaryKeys:(NSDictionary *)primaryKeys mapping:(NSDictionary *)mapping httpMethod:(NSString *)httpMethod path:(NSString *)path identifier:(NSString *)identifier inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    
//    NSLog(@"2 MAP DATAOBJECT: %@", dataObject);
//    NSLog(@"2 PRIMARY KEYS (%@): %@", path, primaryKeys);
//    NSLog(@"2 MAPPING (%@): %@", path, mapping);
    
    __block NSManagedObject *mappedManagedObject;
    
    if ([managedObjectID isKindOfClass:[NSManagedObjectID class]]) {
        mappedManagedObject = [managedObjectContext objectWithID:managedObjectID];
    }
    
    @try {
        
        // Primary keys
        __block NSPredicate *primaryKeysWithValuePredicate;
        __block NSMutableDictionary *primaryKeysWithValue = [[NSMutableDictionary alloc] init];
        [primaryKeys enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
            id object = [dataObject valueForKeyPath:value];
            
            if (! [object isKindOfClass:[NSString class]]) {
                object = [object stringValue];
            }
            
            if (object) {
                [primaryKeysWithValue setObject:object forKey:key];
            }
        }];
        
        if (primaryKeysWithValue.count > 0) {
            primaryKeysWithValuePredicate = [NSManagedObject predicateFromDictionary:primaryKeysWithValue options:NSCaseInsensitivePredicateOption];
        } else {
            return nil;
        }
        
        // Check if ManagedObject exists
        if (! mappedManagedObject && primaryKeysWithValuePredicate) {
            NSArray *managedObjects = [self where:primaryKeysWithValuePredicate inContext:managedObjectContext];
            mappedManagedObject = managedObjects.firstObject;
        }
        
        // Create ManagedObject if it doesn't exists
        if (! mappedManagedObject) {
            mappedManagedObject = [self create:primaryKeysWithValue inContext:managedObjectContext];
        }
        
        // Pre processing block
        MXMappingPreProcessingBlock preProcessingBlock = [mappedManagedObject preProcessingBlock];
        if (preProcessingBlock){
            preProcessingBlock(dataObject, mappedManagedObject, path, httpMethod, identifier);
        }
        
        NSDictionary *attributes = [mappedManagedObject.entity attributesByName];
        NSDictionary *relationships = [mappedManagedObject.entity relationshipsByName];
        
        // Apply mapping {local:remote}
        __block BOOL mapped = NO;
        BOOL shouldMap = [self shouldMapManagedObject:mappedManagedObject dataObject:dataObject primaryKeys:primaryKeys mapping:mapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
        
        if (shouldMap) {
            [mapping enumerateKeysAndObjectsUsingBlock:^(NSString *attributeKey, NSString *remoteKeyPath, BOOL *stop) {
                
                id remoteValue = [dataObject valueForKeyPath:remoteKeyPath];
                
                if (remoteValue) {
                    NSRelationshipDescription *attribute = [attributes objectForKey:attributeKey];
                    NSRelationshipDescription *relationship = [relationships objectForKey:attributeKey];
                    
                    if (attribute) {
                        mapped = [mappedManagedObject setValue:remoteValue forAttribute:attributeKey withIdentifier:identifier];
                    } else if (relationship) {
                        Class relationshipClass = NSClassFromString(relationship.destinationEntity.managedObjectClassName);
                        
                        if ([relationshipClass respondsToSelector:@selector(primaryKeysForPath:pathExtension:httpMethod:identifier:)] &&
                            [relationshipClass respondsToSelector:@selector(mappingForPath:pathExtension:httpMethod:identifier:)])
                        {
                            NSDictionary *relationshipPrimaryKeys = [relationshipClass primaryKeysForPath:path pathExtension:nil httpMethod:httpMethod identifier:identifier];
                            NSDictionary *relationshipMapping = [relationshipClass mappingForPath:path pathExtension:nil httpMethod:httpMethod identifier:identifier];
                            
                            // remoteValue => Array or Dictionary
                            
                            // TODO: Relationship
                            if ([relationship isToMany]) {
                                NSMutableSet *relationshipSet = [[NSMutableSet alloc] init];
                                
                                if ([remoteValue isKindOfClass:[NSArray class]]) {
                                    [remoteValue enumerateObjectsUsingBlock:^(id remoteDataObject, NSUInteger idx, BOOL *stop) {
                                        
                                        //Create Dictionary if Array contains just Strings
                                        if (! [remoteDataObject isKindOfClass:[NSDictionary class]]) {
                                            if ([remoteDataObject isKindOfClass:[NSString class]]) {
                                                NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
                                                [relationshipPrimaryKeys enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                                                    [mutableDictionary setObject:remoteDataObject forKey:obj];
                                                }];
                                                remoteDataObject = [mutableDictionary copy];
                                            }
                                        }
                                        
                                        if ([(NSDictionary *)remoteValue count] > 0) {
                                            NSManagedObject *relationshipManagedObject = [relationshipClass mapManagedObjectID:nil withDataObject:remoteDataObject primaryKeys:relationshipPrimaryKeys mapping:relationshipMapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
                                            
                                            if (relationshipManagedObject) {
                                                [relationshipSet addObject:relationshipManagedObject];
                                            }
                                        }
                                    }];
                                } else if ([remoteValue isKindOfClass:[NSDictionary class]]) {
                                    if ([(NSDictionary *)remoteValue count] > 0) {
                                        NSManagedObject *relationshipManagedObject = [relationshipClass mapManagedObjectID:nil withDataObject:remoteValue primaryKeys:relationshipPrimaryKeys mapping:relationshipMapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
                                        
                                        if (relationshipManagedObject) {
                                            [relationshipSet addObject:relationshipManagedObject];
                                        }
                                    }
                                } else if ([remoteValue isKindOfClass:[NSNull class]]) {
                                    mapped = [mappedManagedObject setValue:remoteValue forAttribute:attributeKey withIdentifier:identifier];
                                }
                                
                                mapped = [mappedManagedObject setValue:relationshipSet forAttribute:attributeKey withIdentifier:identifier];
                            } else {
                                NSManagedObject *relationshipManagedObject;
                                
                                if ([remoteValue isKindOfClass:[NSArray class]]) {
                                    NSArray *remoteValueArray = (NSArray *)remoteValue;
                                    remoteValue = remoteValueArray.lastObject;
                                    
                                    if ([(NSDictionary *)remoteValue count] > 0) {
                                        relationshipManagedObject = [relationshipClass mapManagedObjectID:nil withDataObject:remoteValue primaryKeys:relationshipPrimaryKeys mapping:relationshipMapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
                                    }
                                } else if ([remoteValue isKindOfClass:[NSDictionary class]]) {
                                    if ([(NSDictionary *)remoteValue count] > 0) {
                                        relationshipManagedObject = [relationshipClass mapManagedObjectID:nil withDataObject:remoteValue primaryKeys:relationshipPrimaryKeys mapping:relationshipMapping httpMethod:httpMethod path:path identifier:identifier inManagedObjectContext:managedObjectContext];
                                    }
                                } else if ([remoteValue isKindOfClass:[NSNull class]]) {
                                    mapped = [mappedManagedObject setValue:remoteValue forAttribute:attributeKey withIdentifier:identifier];
                                } else if ([remoteValue isKindOfClass:[NSString class]]) {
                                    
                                    __block NSPredicate *relationshipPrimaryKeysWithValuePredicate;
                                    __block NSMutableDictionary *relationshipPrimaryKeysWithValue = [[NSMutableDictionary alloc] init];
                                    
                                    [relationshipPrimaryKeys enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
                                        [relationshipPrimaryKeysWithValue setObject:remoteValue forKey:key];
                                    }];
                                    
                                    if (relationshipPrimaryKeysWithValue.count > 0) {
                                        relationshipPrimaryKeysWithValuePredicate = [NSManagedObject predicateFromDictionary:relationshipPrimaryKeysWithValue options:NSCaseInsensitivePredicateOption];
                                    }
                                    
                                    if (relationshipPrimaryKeysWithValuePredicate) {
                                        NSArray *relationshipManagedObjects = [self where:relationshipPrimaryKeysWithValuePredicate inContext:managedObjectContext];
                                        relationshipManagedObject = relationshipManagedObjects.firstObject;
                                    }
                                }
                                
                                if (relationshipManagedObject) {
                                    mapped = [mappedManagedObject setValue:relationshipManagedObject forAttribute:attributeKey withIdentifier:identifier];
                                }
                            }
                        }
                    }
                }
            }];
        }
        
        // Post processing block
        MXMappingPostProcessingBlock postProcessingBlock = [mappedManagedObject postProcessingBlock];
        if (postProcessingBlock){
            postProcessingBlock(dataObject, mappedManagedObject, path, httpMethod, identifier, mapped);
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        return mappedManagedObject;
    }
}

@end
