//
//  MXHTTPSessionManager.h
//  Framework
//
//  Created by Michael Loistl on 17/03/2014.
//  
//

#import "AFHTTPSessionManager.h"

@class MXHTTPSessionManager;

@protocol MXHTTPSessionManagerDelegate <NSObject>

@required
- (NSString *)baseURLForHTTPSessionManager:(MXHTTPSessionManager *)sender;

@end

@interface MXHTTPSessionManager : AFHTTPSessionManager

@property (nonatomic, assign, getter=isSuspended) BOOL suspended;

@property (nonatomic, weak) id<MXHTTPSessionManagerDelegate> delegate;

+ (MXHTTPSessionManager *)sharedManager;

- (void)cancelAllDataTasks;
- (void)suspendAllDataTasks;
- (void)resumeAllDataTasks;
- (void)sessionDataTaskIsQueuedForRequest:(NSURLRequest *)request httpMethod:(NSString *)httpMethod completion:(void (^)(BOOL requestIsQueued))completion;

@end
