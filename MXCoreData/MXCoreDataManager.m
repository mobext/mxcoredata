//
//  MXCoreDataManager.h
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import "MXCoreDataManager.h"
#import "MXHTTPSessionManager.h"

@implementation MXCoreDataManager

#pragma mark - Property Getters

@synthesize managedObjectModel = __managedObjectModel;

@synthesize managedObjectContextMain = __managedObjectContextMain;
@synthesize managedObjectContextBackground = __managedObjectContextBackground;
@synthesize managedObjectContextMemory = __managedObjectContextMemory;
@synthesize managedObjectContextStore = __managedObjectContextStore;

@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize memoryStoreCoordinator = __memoryStoreCoordinator;

- (NSManagedObjectModel *) managedObjectModel {
    if (! __managedObjectModel) {
        NSParameterAssert(self.storeName);
        
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:self.storeName
                                                  withExtension:@"momd"];
        
        __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }

    return __managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (! __persistentStoreCoordinator) {
        NSParameterAssert(self.storeName);
        
        __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",self.storeName]];
        
        NSDictionary *options = @{
                                  NSInferMappingModelAutomaticallyOption : @(YES),
                                  NSMigratePersistentStoresAutomaticallyOption: @(YES)
                                  };
        
        NSError *error = nil;
        if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                        configuration:nil
                                                                  URL:storeURL
                                                              options:options
                                                                error:&error]) {
            NSLog(@"Store Coordinator - Unresolved error %@, %@", error, [error userInfo]);
        }
    }
    
    return __persistentStoreCoordinator;
}

- (NSPersistentStoreCoordinator *)memoryStoreCoordinator {
    if (! __memoryStoreCoordinator) {
        __memoryStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        NSDictionary *options = @{
                                  NSInferMappingModelAutomaticallyOption : @(YES),
                                  NSMigratePersistentStoresAutomaticallyOption: @(YES)
                                  };
        
        NSError *error = nil;
        if (![__memoryStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType
                                                        configuration:nil
                                                                  URL:nil
                                                              options:options
                                                                error:&error]) {
            NSLog(@"Store Coordinator - Unresolved error %@, %@", error, [error userInfo]);
        }
    }
    
    return __memoryStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContextBackground {
    if (! __managedObjectContextBackground) {
        __managedObjectContextBackground = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        __managedObjectContextBackground.parentContext = self.managedObjectContextMain;
        
        NSUndoManager *undoManager = [[NSUndoManager alloc] init];
        [__managedObjectContextBackground setUndoManager:undoManager];
    }
    
    return __managedObjectContextBackground;
}

- (NSManagedObjectContext *)managedObjectContextMain {
    if (! __managedObjectContextMain) {
        __managedObjectContextMain = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        __managedObjectContextMain.parentContext = self.managedObjectContextStore;

        NSUndoManager *undoManager = [[NSUndoManager alloc] init];
        [__managedObjectContextMain setUndoManager:undoManager];
    }
    
    return __managedObjectContextMain;
}

- (NSManagedObjectContext *)managedObjectContextStore {
    if (! __managedObjectContextStore) {
        __managedObjectContextStore = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [__managedObjectContextStore setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    }
    
    return __managedObjectContextStore;
}

- (NSManagedObjectContext *)managedObjectContextMemory {
    if (! __managedObjectContextMemory) {
        __managedObjectContextMemory = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [__managedObjectContextMemory setPersistentStoreCoordinator:self.memoryStoreCoordinator];
    }
    
    return __managedObjectContextMemory;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (NSMutableSet *)syncingManagedObjects {
    if (! _syncingManagedObjects) {
        _syncingManagedObjects = [[NSMutableSet alloc] init];
    }
    
    return _syncingManagedObjects;
}

#pragma mark - Methods

+ (MXCoreDataManager *)sharedManager {
	static dispatch_once_t pred;
	static MXCoreDataManager *sharedManager = nil;
    
	dispatch_once(&pred, ^{
        sharedManager = [[self alloc] init];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MXHTTPSessionManager sharedManager];
        });
    });
    
	return sharedManager;
}

+ (NSManagedObjectContext *)managedObjectContextMain {
    return [MXCoreDataManager sharedManager].managedObjectContextMain;
}

+ (NSManagedObjectContext *)managedObjectContextBackground {
    return [MXCoreDataManager sharedManager].managedObjectContextBackground;
}

+ (NSManagedObjectContext *)managedObjectContextMemory {
    return [MXCoreDataManager sharedManager].managedObjectContextMemory;
}

- (void)performFetchWithController:(NSFetchedResultsController *)fetchedResultsController {
    [fetchedResultsController performSelectorOnMainThread:@selector(performFetch:) withObject:nil waitUntilDone:YES modes:@[NSRunLoopCommonModes]];
}

#pragma mark - Init

- (id)init {
    self = [super init];
    
    if (self) {
        self.backgroundQueue = dispatch_queue_create("com.mobext.CoreDataManager", 0);
    }
    
    return self;
}

@end
