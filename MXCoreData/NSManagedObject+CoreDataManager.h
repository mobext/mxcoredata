//
//  MXCoreDataManager.h
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (CoreDataManager)

#pragma mark - Methods

- (NSDateFormatter *)dateFormatterForAttribute:(NSString *)attribute identifier:(NSString *)identifier;

+ (NSPredicate *)predicateFromDictionary:(NSDictionary *)conditions options:(NSComparisonPredicateOptions)options;

#pragma mark - Default Context

- (void)delete;
+ (void)deleteAll;
+ (void)deleteAllInEntity:(NSString *)entity;

+ (id)create;
+ (id)create:(NSDictionary *)attributes;

- (void)update:(NSDictionary *)attributes;

- (BOOL)setValue:(id)value forAttribute:(NSString *)attribute withIdentifier:(NSString *)identifier;

+ (NSArray *)all;
+ (NSArray *)allInEntity:(NSString *)entity;

+ (NSArray *)where:(id)condition;
+ (NSArray *)where:(id)condition inEntity:(NSString *)entity;

+ (BOOL)key:(NSString *)key inEntity:(NSString *)entity;
+ (BOOL)attribute:(NSString *)attribute inEntity:(NSString *)entity;
+ (BOOL)relationship:(NSString *)relationship inEntity:(NSString *)entity;

#pragma mark - Custom Context

+ (Class)classForEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context;

+ (id)createInContext:(NSManagedObjectContext *)context;
+ (id)create:(NSDictionary *)attributes inContext:(NSManagedObjectContext *)context;

+ (void)deleteAllInContext:(NSManagedObjectContext *)context;
+ (void)deleteAllInEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context;

+ (NSArray *)allInContext:(NSManagedObjectContext *)context;
+ (NSArray *)allInEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context;

+ (NSArray *)where:(id)condition inContext:(NSManagedObjectContext *)context;
+ (NSArray *)where:(id)condition inEntity:(NSString *)entity inContext:(NSManagedObjectContext *)context;

+ (BOOL)key:(NSString *)key inEntity:(NSString *)entity inManagedObjectContext:(NSManagedObjectContext *)context;
+ (BOOL)attribute:(NSString *)attribute inEntity:(NSString *)entity inManagedObjectContext:(NSManagedObjectContext *)context;
+ (BOOL)relationship:(NSString *)relationship inEntity:(NSString *)entity inManagedObjectContext:(NSManagedObjectContext *)context;

#pragma mark - Naming

+ (NSString *)entityName;

@end