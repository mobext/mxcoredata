//
//  NSManagedObjectContext+CoreDataManager.m
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import "NSManagedObjectContext+CoreDataManager.h"
#import "MXCoreDataManager.h"

#define kIsSaving @"com.mobext.isSaving"

@implementation NSManagedObjectContext (CoreDataManager)

+ (NSManagedObjectContext *)defaultContext {
    return [[MXCoreDataManager sharedManager] managedObjectContextMain];
}

+ (NSManagedObjectContext *)contextWithConcurrencyType:(NSManagedObjectContextConcurrencyType)concurrencyType parentContext:(NSManagedObjectContext *)parentContext {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:concurrencyType];
    context.parentContext = (parentContext) ? parentContext : [MXCoreDataManager managedObjectContextMain];
    return context;
}

- (BOOL)save {
    return [self saveParentContext:YES];
}

- (BOOL)saveParentContext:(BOOL)saveParentContext {
    
    if (! [self.userInfo objectForKey:kIsSaving]) {
        
        [self.userInfo setObject:@YES forKey:kIsSaving];
        
        __block NSError *error = nil;
        NSManagedObjectContext *saveContext = self;
        __block BOOL allSuccess = (saveContext) ? YES : NO;
        
        while (saveContext) {
            if ([saveContext hasChanges]) {
                [saveContext performBlockAndWait:^{
                    BOOL success = [saveContext save:&error];
                    
                    if (! success) {
                        allSuccess = success;
                        NSLog(@"SAVING ERROR: %@", error);
                    }
                }];
            }
            
            saveContext = (saveParentContext) ? saveContext.parentContext : nil;
        }
        
        [self.userInfo removeObjectForKey:kIsSaving];
        
        return allSuccess;
    }
    return NO;
}

- (void)logContext {
    NSLog(@"%@", [self stringForContext:self]);
}

- (NSString *)stringForContext:(NSManagedObjectContext *)managedObjectContext {
    if (managedObjectContext == [MXCoreDataManager sharedManager].managedObjectContextMain) {
        return @"##### MAIN CONTEXT #####";
    } else if (managedObjectContext == [MXCoreDataManager sharedManager].managedObjectContextBackground) {
        return @"##### BACKGROUND CONTEXT #####";
    } else if (managedObjectContext == [MXCoreDataManager sharedManager].managedObjectContextMemory) {
        return @"##### MEMORY CONTEXT #####";
    } else {
        if (managedObjectContext.concurrencyType == NSConfinementConcurrencyType) {
            return @"##### UNKNOWN CONTEXT (NSConfinementConcurrencyType) #####";
        } else if (managedObjectContext.concurrencyType == NSPrivateQueueConcurrencyType) {
            return @"##### UNKNOWN CONTEXT (NSPrivateQueueConcurrencyType) #####";
        } else if (managedObjectContext.concurrencyType == NSMainQueueConcurrencyType) {
            return @"##### UNKNOWN CONTEXT (NSMainQueueConcurrencyType) #####";
        }
    }
    
    return nil;
}

#pragma mark - Deprecated

- (void)saveWithCompletion:(void (^)(BOOL success))completion {
    [self saveParentContext:YES withCompletion:^(BOOL success) {
        completion(success);
    }];
}

- (void)saveParentContext:(BOOL)saveParentContext withCompletion:(void (^)(BOOL success))completion {
    __block NSError *error = nil;
    __block BOOL allSuccess = YES;
    
    NSManagedObjectContext *saveContext = self;
    
    while (saveContext) {
        
        if ([saveContext hasChanges]) {
            
            //            [self logContext:saveContext];
            
            BOOL success = [saveContext save:&error];
            
            if (! success) {
                allSuccess = success;
                NSLog(@"SAVING ERROR: %@", error);
            }
        }
        
        saveContext = (saveParentContext) ? saveContext.parentContext : nil;
        
        if (! saveContext) {
            completion(allSuccess);
        }
    }
}

@end
