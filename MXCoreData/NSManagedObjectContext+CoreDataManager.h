//
//  NSManagedObjectContext+CoreDataManager.h
//  Framework
//
//  Created by Michael Loistl on 12/04/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (CoreDataManager)

+ (NSManagedObjectContext *)defaultContext;
+ (NSManagedObjectContext *)contextWithConcurrencyType:(NSManagedObjectContextConcurrencyType)concurrencyType parentContext:(NSManagedObjectContext *)parentContext;

- (BOOL)save;
- (BOOL)saveParentContext:(BOOL)saveParentContext;
- (void)logContext;

// Deprecated
- (void)saveWithCompletion:(void (^)(BOOL success))completion;
- (void)saveParentContext:(BOOL)saveParentContext withCompletion:(void (^)(BOOL success))completion;

@end